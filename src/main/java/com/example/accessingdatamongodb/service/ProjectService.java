package com.example.accessingdatamongodb.service;


import com.example.accessingdatamongodb.dao.model.Project;
import com.example.accessingdatamongodb.dao.repository.ProjectRepository;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProjectService {

  @Autowired
  ProjectRepository projectRepository;

  public  List<Project>  getAllProject(){
    final List<Project> projects = projectRepository.findAll();
    return projects;

  }

  public Project save(Project project) {
    project.setId(1L);
    final Project projectSaved = projectRepository.save(project);
    return projectSaved;
  }

  public Project getProjectById(Long id) {
    final Optional<Project> project = projectRepository.findById(id);
    return project.get();
  }

  public Project modifierProjet(Long id,
      Project nouveauProject) {
    return projectRepository.findById(id).map(project -> {
      project.setTitre(nouveauProject.getTitre());
      project.setChefDeProjet(nouveauProject.getChefDeProjet());
      project.setTechnologies(nouveauProject.getTechnologies());
      return projectRepository.save(project);
    } ).orElseGet(() -> {
      nouveauProject.setId(id);
      return projectRepository.save(nouveauProject);
    });
  }

  public void deleteProject(Long id) {
    projectRepository.deleteById(id);
  }
}
