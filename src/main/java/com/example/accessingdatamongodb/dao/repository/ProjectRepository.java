package com.example.accessingdatamongodb.dao.repository;

import com.example.accessingdatamongodb.dao.model.Project;
import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ProjectRepository extends MongoRepository<Project, Long> {

  @Override
  List<Project> findAll();

  @Override
  Project save(Project project);
}
