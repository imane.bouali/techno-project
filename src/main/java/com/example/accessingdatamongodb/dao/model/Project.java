package com.example.accessingdatamongodb.dao.model;


import java.util.Set;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Project {

  @Id
  public Long id;

  public String titre;

  public String chefDeProjet;

  public Set<String> technologies;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getTitre() {
    return titre;
  }

  public void setTitre(String titre) {
    this.titre = titre;
  }

  public String getChefDeProjet() {
    return chefDeProjet;
  }

  public void setChefDeProjet(String chefDeProjet) {
    this.chefDeProjet = chefDeProjet;
  }

  public Set<String> getTechnologies() {
    return technologies;
  }

  public void setTechnologies(Set<String> technologies) {
    this.technologies = technologies;
  }
}
