package com.example.accessingdatamongodb.api;


import com.example.accessingdatamongodb.dao.model.Project;
import com.example.accessingdatamongodb.service.ProjectService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/projects")
public class ProjectController {

  @Autowired
  ProjectService projectService;

  @GetMapping("")
  public List<Project> getAllProjects(){
    final List<Project> projects = projectService.getAllProject();
    return projects;

  }

  /* Implementer les diffrents operations CRUD */

  @PostMapping("")
  public Project createProject(@RequestBody Project project) {
    return projectService.save(project);
  }

  @GetMapping("/{id}")
  public Project getProjectById(@PathVariable("id") Long id){
    return projectService.getProjectById(id);
  }

  @PutMapping("/{id}")
  public Project modifierProjet(@RequestBody Project nouveauProject,@PathVariable("id") Long id){
    return projectService.modifierProjet(id, nouveauProject);
  }

  @DeleteMapping("/{id}")
  void deleteProject(@PathVariable("id") Long id) {
    projectService.deleteProject(id);
  }

  // Inmlementer la recherche



}
